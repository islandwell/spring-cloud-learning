package com.zyw.demoprovider.comsoer.service;

import com.zyw.demoprovider.comsoer.client.dto.model.UserModel;

public interface TestService {
    /**
     * 查询用户信息
     * @param name
     * @return
     */
    UserModel queryUserInfo(String name);
}
