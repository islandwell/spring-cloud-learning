package com.zyw.demoprovider.comsoer.controller;

import com.zyw.demoprovider.comsoer.client.TestApi;
import com.zyw.demoprovider.comsoer.client.dto.CommonResponse;
import com.zyw.demoprovider.comsoer.client.dto.model.UserModel;
import com.zyw.demoprovider.comsoer.service.TestService;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
public class TestController implements TestApi {
    @Resource
    private TestService testService;

    @Override
    public Map<String, Object> getPaymentById(Long id) {
        Map<String, Object> map = new HashMap<>();
        map.put("msg", "success");
        map.put("data", id);
        return map;
    }

    /**
     * 查询用户信息
     *
     * @param name
     * @return
     */
    @Override
    public CommonResponse<UserModel> queryUserInfo(String name) {
        return new CommonResponse<UserModel>(testService.queryUserInfo(name));
    }
}
