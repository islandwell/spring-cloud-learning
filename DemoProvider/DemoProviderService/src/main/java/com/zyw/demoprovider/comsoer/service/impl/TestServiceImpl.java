package com.zyw.demoprovider.comsoer.service.impl;

import com.zyw.demoprovider.comsoer.client.dto.model.UserModel;
import com.zyw.demoprovider.comsoer.service.TestService;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {
    /**
     * 查询用户信息
     *
     * @param name
     * @return
     */
    @Override
    public UserModel queryUserInfo(String name) {
        UserModel userModel = new UserModel();
        userModel.setUsername(name);
        userModel.setPhone("18909871901");
        userModel.setRole("客户经理");
        return userModel;
    }
}
