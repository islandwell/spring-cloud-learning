package com.zyw.demoprovider.comsoer.client.dto;

import java.io.Serializable;

public interface Message extends Serializable {
    String getCode();

    String getMsg();
}
