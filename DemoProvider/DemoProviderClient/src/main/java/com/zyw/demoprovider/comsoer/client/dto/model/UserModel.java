package com.zyw.demoprovider.comsoer.client.dto.model;

import lombok.Data;

@Data
public class UserModel {
    private String username;
    private String phone;
    private String role;
}
