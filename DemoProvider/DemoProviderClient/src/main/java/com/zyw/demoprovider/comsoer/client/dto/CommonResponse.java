package com.zyw.demoprovider.comsoer.client.dto;

public class CommonResponse<T> {
    private String respCode;
    private String respMsg;
    private T body;
    public static final CommonResponse SUCCESS;

    public String getRespCode() {
        return this.respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespMsg() {
        return this.respMsg;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }

    public T getBody() {
        return this.body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    public CommonResponse() {
    }

    public CommonResponse(Message message) {
        this.respCode = message.getCode();
        this.respMsg = message.getMsg();
    }

    public CommonResponse(T body) {
        this.body = body;
        this.respCode = BaseMessage.SUCCESS.getCode();
        this.respMsg = BaseMessage.SUCCESS.getMsg();
    }

    public CommonResponse(Message message, T body) {
        this.body = body;
        this.respCode = message.getCode();
        this.respMsg = message.getMsg();
    }

    public CommonResponse(String code, String msg, T body) {
        this.body = body;
        this.respCode = code;
        this.respMsg = msg;
    }

    public Boolean success() {
        return BaseMessage.SUCCESS.getCode().equals(this.respCode) ? true : false;
    }

    static {
        SUCCESS = new CommonResponse(BaseMessage.SUCCESS);
    }
}