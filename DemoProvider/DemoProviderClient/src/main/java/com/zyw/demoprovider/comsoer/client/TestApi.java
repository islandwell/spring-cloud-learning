package com.zyw.demoprovider.comsoer.client;

import com.zyw.demoprovider.comsoer.client.dto.CommonResponse;
import com.zyw.demoprovider.comsoer.client.dto.model.UserModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@RequestMapping(value = "")
public interface TestApi {
    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/payment/get/{id}")
    Map<String, Object> getPaymentById(@PathVariable("id") Long id);


    /**
     * 查询用户信息
     *
     * @param name
     * @return
     */
    @GetMapping(value = "/user/info/query/{name}")
    CommonResponse<UserModel> queryUserInfo(@PathVariable("name") String name);

}
