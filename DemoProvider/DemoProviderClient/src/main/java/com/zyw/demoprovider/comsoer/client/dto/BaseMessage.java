package com.zyw.demoprovider.comsoer.client.dto;

public enum BaseMessage implements Message {
    SUCCESS("10000", "success"),
    ERROR_PARAM("90001", "error_param"),
    COMMUNICATION_EXCEPTION("90002", "communication_exception"),
    HTTP_STATUS_EXCEPTION("90003", "http_status_exception"),
    PARSE_JSON_EXCEPTION("90004", "parse_json_exception"),
    SYSTEM_EXCEPTION("99999", "system_exception");

    private String code;
    private String msg;

    private BaseMessage(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }
}
