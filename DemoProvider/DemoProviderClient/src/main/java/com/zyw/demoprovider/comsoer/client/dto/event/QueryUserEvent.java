package com.zyw.demoprovider.comsoer.client.dto.event;

import lombok.Data;

@Data
public class QueryUserEvent {
    private String phone;
}
